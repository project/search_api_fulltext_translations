<?php

namespace Drupal\search_api_fulltext_translations\Plugin\search_api\processor;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\search_api\Plugin\PluginFormTrait;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\SearchApiException;
use Drupal\search_api\Utility\DataTypeHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Adds a highlighted excerpt to results and highlights returned fields.
 *
 * This processor won't run for queries with the "basic" processing level set.
 *
 * @SearchApiProcessor(
 *   id = "add_fulltext_translations",
 *   label = @Translation("Add fulltext translations"),
 *   description = @Translation("Adds translated values to every existing fulltext field."),
 *   stages = {
 *     "preprocess_index" = -50,
 *   }
 * )
 */
class AddFulltextTranslations extends ProcessorPluginBase implements PluginFormInterface {

  use PluginFormTrait;

  /**
   * The data type helper.
   *
   * @var \Drupal\search_api\Utility\DataTypeHelperInterface|null
   */
  protected $dataTypeHelper;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var static $processor */
    $processor = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $processor->setLanguageManager($container->get('language_manager'));

    return $processor;
  }

  /**
   * Retrieves the language manager.
   *
   * @return \Drupal\Core\Language\LanguageManagerInterface
   *   The language manager.
   */
  public function getLanguageManager() {
    return $this->languageManager;
  }

  /**
   * Sets the language manager.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The new language manager.
   *
   * @return $this
   */
  public function setLanguageManager(LanguageManagerInterface $languageManager) {
    $this->languageManager = $languageManager;
    return $this;
  }

  /**
   * Retrieves the data type helper.
   *
   * @return \Drupal\search_api\Utility\DataTypeHelperInterface
   *   The data type helper.
   */
  public function getDataTypeHelper() {
    return $this->dataTypeHelper ?: \Drupal::service('search_api.data_type_helper');
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Sets the data type helper.
   *
   * @param \Drupal\search_api\Utility\DataTypeHelperInterface $data_type_helper
   *   The new data type helper.
   *
   * @return $this
   */
  public function setDataTypeHelper(DataTypeHelperInterface $data_type_helper) {
    $this->dataTypeHelper = $data_type_helper;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function preprocessIndexItems(array $items) {

    /** @var \Drupal\search_api\Item\ItemInterface $item */
    foreach ($items as $item) {
      try {
        /** @var Drupal\Core\Entity\ContentEntityInterface $entity */
        $entity = $item->getOriginalObject()->getValue();
      }
      catch (SearchApiException $e) {
        return;
      }
      if (!($entity instanceof ContentEntityInterface)) {
        return;
      }
      $fields = $item->getFields();

      foreach ($fields as $field) {
        if ($field->getType() == 'text') {
          $field_name = $field->getPropertyPath();

          $languages = $this->languageManager->getLanguages();
          foreach ($languages as $langcode => $language) {
            $field_keys = explode(":", $field_name);
            // Eg body.
            $fulltext_field = $field_keys[0];
            // Eg html.
            $fulltext_field_key = $field_keys[1] ?? NULL;
            $translation = $entity->language()->getId() != $langcode && $entity->hasTranslation($langcode) ? $entity->getTranslation($langcode) : NULL;

            if ($translation && $translation->hasField($fulltext_field) && !$translation->get($fulltext_field)->isEmpty()) {
              $translated_field_value = $fulltext_field_key ? $translation->get($fulltext_field)->{$fulltext_field_key} : $translation->get($fulltext_field)->value;
              $field->addValue($translated_field_value);
            }
          }

        }
      }
    }
  }

}
