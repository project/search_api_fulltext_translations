CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
This module provides a Search API processor to add all translations to the indexed values for all fulltext fields. This allows for multilingual searching,

For example: There is one piece of content with two translations: en and de. The en translation has the title 'Result' and the de translation has the title 'Ergebnis'.
When the search results are only showing the en translations of content, with the fulltext translation processor enabled,
a fulltext search for "Ergebnis" will return the en version of the content, as "Ergebnis" is an indexed value against this field.

REQUIREMENTS
------------
This module requires the following modules:

 * Search API (https://www.drupal.org/project/search_api)

INSTALLATION
------------
Install as you would normally install a contributed Drupal module. For further
information, see:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-modules

CONFIGURATION
-------------
After installation, create a search index as normal, and in the processors configuration add the 'Add fulltext translations' processor.

MAINTAINERS
-----------
Current maintainers:
  * Nick O'Sullivan (nbaosullivan) - https://www.drupal.org/u/nbaosullivan
